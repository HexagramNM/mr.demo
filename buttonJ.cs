﻿using UnityEngine;
using System.Collections;

public class buttonJ : MonoBehaviour {
    GameObject ARC;
    GameObject ButtonJ;
    GameObject ButtonE;
    public void OnClick()
    {
        ARC = GameObject.Find("ARCamera");
        ButtonJ = GameObject.Find("ButtonJ");
        ButtonE = GameObject.Find("ButtonE");
        ButtonJ.SetActive(false);
        ButtonE.SetActive(false);
        ARC.GetComponent<advertisementstart>().SetLang('J');
    }
}
