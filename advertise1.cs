﻿using UnityEngine;
using System.Collections;

public class advertise1 : advertisementscript{
    GameObject textcreate;
    GameObject textplay;
    GameObject poster;
    MeshRenderer postermesh;
    GameObject place;
    MeshRenderer placemesh;
    GameObject placestr;
    string placeJ;
    string placeE;
    GameObject rogo;
    GameObject webswitch;
    GameObject webstr;
    Vector3 createinitpos;
    Vector3 playinitpos;
    Vector3 rogoinitpos;
    float webswitchcounter;
    Vector3 defaultswitchscale;
    // Use this for initialization
    public override void Start () {
        base.Start();

        textcreate = GameObject.Find("create");
        textplay = GameObject.Find("play");
        createinitpos = textcreate.transform.localPosition - new Vector3(0.0f, 0.5f, 0.0f);
        playinitpos = textplay.transform.localPosition - new Vector3(0.0f, 0.5f, 0.0f);
        textcreate.SetActive(false);
        textplay.SetActive(false);
        poster = GameObject.Find("poster");
        postermesh = poster.GetComponent<MeshRenderer>();
        postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        poster.SetActive(false);
        place = GameObject.Find("Place");
        placemesh = place.GetComponent<MeshRenderer>();
        placemesh.material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        place.SetActive(false);
        placestr = GameObject.Find("placestr");
        placestr.SetActive(false);
        placeE = "Clubroom:\n    Jukuseikaikan\n    4F 401Right";
        placeJ = "部室:\n    塾生会館\n    4階 401R";
        rogo = GameObject.Find("rogo");
        rogo.SetActive(false);
        rogoinitpos = rogo.transform.localPosition - new Vector3(0.0f, 1.0f, 0.0f);
        webswitch = GameObject.Find("webswitch");
        webstr = GameObject.Find("webstr");
        webswitch.SetActive(false);
        webswitchcounter = 0.0f;
        defaultswitchscale = webswitch.transform.localScale;
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
	
        if (getFlag())
        {
            if (JEflag == 0)
            {
                if (getCount() < 0.5f)
                {
                    textcreate.SetActive(true);
                    textcreate.GetComponent<TextMesh>().color = new Color(1.0f, 0.0f, 0.0f, 2.0f * getCount());
                    textplay.SetActive(true);
                    textplay.GetComponent<TextMesh>().color = new Color(0.0f, 1.0f, 1.0f, 2.0f * getCount());
                    poster.SetActive(true);
                    postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, getCount() * 2.0f);
                    textcreate.transform.localPosition += new Vector3(0.0f, 1.0f * Time.deltaTime, 0.0f);
                    textplay.transform.localPosition += new Vector3(0.0f, 1.0f * Time.deltaTime, 0.0f);
                    place.SetActive(true);
                    placemesh.material.color = new Color(1.0f, 1.0f, 1.0f, 1.4f * getCount());
                    placestr.SetActive(true);
                    placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 2.0f * getCount());
                }
                else if (getCount() < 1.0f )
                {
                    rogo.SetActive(true);
                    rogo.transform.localPosition += new Vector3(0.0f, 2.0f * Time.deltaTime, 0.0f);

                }
                else if (getCount() < 1.5f)
                {
                    rogo.SetActive(true);
                    rogo.transform.localPosition += new Vector3(-5.0f * (1.5f-getCount()) * Time.deltaTime, 0.0f, 0.0f);

                } else
                {
                    webswitch.SetActive(true);
                    if (webswitchcounter <= 0.0f)
                    {
                        webswitch.transform.localScale = new Vector3(0.1f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                    }
                    else
                    {
                        webswitch.transform.localScale = new Vector3(0.02f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(0.5f, 0.5f, 0.5f);
                        webswitchcounter -= Time.deltaTime;
                    }
                    if (Input.GetMouseButtonDown(0) && webswitchcounter <= 0.0f)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit = new RaycastHit();
                        if (JEflag != -1 && Physics.Raycast(ray, out hit))
                        {
                            GameObject obj = hit.collider.gameObject;
                            if (obj == webswitch)
                            {
                                Application.OpenURL("http://www.d-vertex.com/circle/");
                                webswitchcounter = 1.0f;
                            }
                        }
                    }
                }
            } else if (JEflag == 1)
            {
                if (getCount() < 0.5f)
                {
                    textcreate.SetActive(true);
                    textcreate.GetComponent<TextMesh>().color = new Color(1.0f, 0.0f, 0.0f, 2.0f * getCount());
                    textplay.SetActive(true);
                    textplay.GetComponent<TextMesh>().color = new Color(0.0f, 1.0f, 1.0f, 2.0f * getCount());
                    poster.SetActive(true);
                    postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, getCount() * 2.0f);
                    textcreate.transform.localPosition += new Vector3(0.0f, 1.0f * Time.deltaTime, 0.0f);
                    textplay.transform.localPosition += new Vector3(0.0f, 1.0f * Time.deltaTime, 0.0f);
                    place.SetActive(true);
                    placemesh.material.color = new Color(1.0f, 1.0f, 1.0f, 1.4f * getCount());
                    placestr.SetActive(true);
                    placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 2.0f * getCount());
                }
                else if (getCount() < 1.0f)
                {
                    rogo.SetActive(true);
                    rogo.transform.localPosition += new Vector3(0.0f, 2.0f * Time.deltaTime, 0.0f);

                }
                else if (getCount() < 1.5f)
                {
                    rogo.SetActive(true);
                    rogo.transform.localPosition += new Vector3(-5.0f * (1.5f - getCount()) * Time.deltaTime, 0.0f, 0.0f);

                }
                else
                {
                    webswitch.SetActive(true);
                    if (webswitchcounter <= 0.0f)
                    {
                        webswitch.transform.localScale = new Vector3(0.1f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                    }
                    else
                    {
                        webswitch.transform.localScale = new Vector3(0.02f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(0.5f, 0.5f, 0.5f);
                        webswitchcounter -= Time.deltaTime;
                    }
                    if (Input.GetMouseButtonDown(0) && webswitchcounter <= 0.0f)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit = new RaycastHit();
                        if (JEflag != -1 && Physics.Raycast(ray, out hit))
                        {
                            GameObject obj = hit.collider.gameObject;
                            if (obj == webswitch)
                            {
                                Application.OpenURL("http://www.d-vertex.com/circle/");
                                webswitchcounter = 1.0f;
                            }
                        }
                    }
                }
            }
        } 

	}

    public override void boot(int lang)
    {
        base.boot(lang);
        textcreate.transform.localPosition = createinitpos;
        textplay.transform.localPosition = playinitpos;
        placemesh.material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        rogo.transform.localPosition = rogoinitpos;
        placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        webswitchcounter = 0.0f;
        if (lang == 0)
        {
            textplay.GetComponent<TextMesh>().text = "ゲームで遊ぼう!!";
            textcreate.GetComponent<TextMesh>().text = "ゲームを創ろう!!";
            placestr.GetComponent<TextMesh>().text = placeJ;
            webswitch.SetActive(true);
            webstr.GetComponent<TextMesh>().text = "ここをタッチ！";
            webswitch.SetActive(false);
        } else
        {
            textplay.GetComponent<TextMesh>().text = "Let's play games!!";
            textcreate.GetComponent<TextMesh>().text = "Let's create games!!";
            placestr.GetComponent<TextMesh>().text = placeE;
            webswitch.SetActive(true);
            webstr.GetComponent<TextMesh>().text = "Touch here!";
            webswitch.SetActive(false);
        }
    }
    public override void finish()
    {
        poster.SetActive(false);
        rogo.SetActive(false);
        textcreate.SetActive(false);
        textplay.SetActive(false);
        textcreate.GetComponent<TextMesh>().color = new Color(1.0f, 0.0f, 0.0f, 0.0f);
        textplay.GetComponent<TextMesh>().color = new Color(0.0f, 1.0f, 1.0f, 0.0f);
        postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        place.SetActive(false);
        placestr.SetActive(false);
        placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        place.SetActive(false);
        webswitch.SetActive(false);
    }
}
