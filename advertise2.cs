﻿using UnityEngine;
using System.Collections;

public class advertise2 : advertisementscript{
    GameObject textgamer;
    GameObject textgamer2;
    GameObject textcheer;
    GameObject poster;
    MeshRenderer postermesh;
    GameObject placestr;
    string placeJ;
    string placeE;
    GameObject rogo;
    GameObject webswitch;
    GameObject webstr;
    Vector3 gamerinitpos;
    Vector3 gamer2initpos;
    Vector3 cheerinitpos;
    Vector3 rogoinitpos;
    float webswitchcounter;
    Vector3 defaultswitchscale;
    // Use this for initialization
    public override void Start () {
        base.Start();

        textgamer = GameObject.Find("gamer");
        textgamer2 = GameObject.Find("gamer2");
        textcheer = GameObject.Find("cheer");
        gamerinitpos = textgamer.transform.localPosition - new Vector3(0.5f, 0.7f, 0.0f);
        gamer2initpos = textgamer2.transform.localPosition - new Vector3(0.5f, 0.7f, 0.0f);
        cheerinitpos = textcheer.transform.localPosition - new Vector3(0.5f, 0.7f, 0.0f);
        textgamer.SetActive(false);
        textgamer2.SetActive(false);
        textcheer.SetActive(false);
        poster = GameObject.Find("poster2");
        postermesh = poster.GetComponent<MeshRenderer>();
        postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        poster.SetActive(false);
        placestr = GameObject.Find("placestr2");
        placestr.SetActive(false);
        placeE = "Clubroom:\n    Jukuseikaikan\n    4F 401Right";
        placeJ = "部室:\n    塾生会館\n    4階 401R";
        rogo = GameObject.Find("rogo");
        rogo.SetActive(false);
        rogoinitpos = rogo.transform.localPosition - new Vector3(0.0f, 1.0f, 0.0f);
        webswitch = GameObject.Find("webswitch2");
        webstr = GameObject.Find("webstr2");
        webswitch.SetActive(false);
        webswitchcounter = 0.0f;
        defaultswitchscale = webswitch.transform.localScale;
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
	
        if (getFlag())
        {
            if (JEflag == 0)
            {
                if (getCount() < 0.5f)
                {
                    textgamer.SetActive(true);
                    textgamer.GetComponent<TextMesh>().color = new Color(1.0f, 0.9f, 0.0f, 2.0f * getCount());
                    textgamer2.SetActive(true);
                    textgamer2.GetComponent<TextMesh>().color = new Color(0.3f, 0.7f, 1.0f, 2.0f * getCount());
                    poster.SetActive(true);
                    postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, 2.0f * getCount());
                    textgamer.transform.localPosition += new Vector3(1.0f * Time.deltaTime, 1.4f * Time.deltaTime, 0.0f);
                    textgamer2.transform.localPosition += new Vector3(1.0f * Time.deltaTime, 1.4f * Time.deltaTime, 0.0f);
                    textgamer.transform.localScale = new Vector3(getCount()/10.0f, getCount() / 10.0f, 0.06f);
                    textgamer2.transform.localScale = new Vector3(getCount() / 10.0f, getCount() / 10.0f, 0.06f);
                    placestr.SetActive(true);
                    placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 2.0f * getCount());
                }
                else if (getCount() < 1.0f )
                {
                    textcheer.SetActive(true);
                    textcheer.GetComponent<TextMesh>().color = new Color(1.0f, 0.45f, 0.4f, 2.0f * (getCount()-0.5f));
                    textcheer.transform.localPosition += new Vector3(1.0f * Time.deltaTime, 1.4f * Time.deltaTime, 0.0f);
                    textcheer.transform.localScale = new Vector3((getCount()-0.5f) / 10.0f, (getCount() - 0.5f) / 10.0f, 0.06f);
                }
                else if (getCount() < 1.5f)
                {
                    rogo.SetActive(true);
                    rogo.transform.localPosition += new Vector3(-2.0f * (1.25f - getCount()) * Time.deltaTime, 8.0f * (1.5f - getCount()) * Time.deltaTime, -2.0f * (1.25f - getCount()) * Time.deltaTime);
                }
                else
                {
                    webswitch.SetActive(true);
                    if (webswitchcounter <= 0.0f)
                    {
                        webswitch.transform.localScale = new Vector3(0.1f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                    }
                    else
                    {
                        webswitch.transform.localScale = new Vector3(0.02f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(0.5f, 0.5f, 0.5f);
                        webswitchcounter -= Time.deltaTime;
                    }
                    if (Input.GetMouseButtonDown(0) && webswitchcounter <= 0.0f)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit = new RaycastHit();
                        if (JEflag != -1 && Physics.Raycast(ray, out hit))
                        {
                            GameObject obj = hit.collider.gameObject;
                            if (obj == webswitch)
                            {
                                Application.OpenURL("http://www.d-vertex.com/circle/");
                                webswitchcounter = 1.0f;
                            }
                        }
                    }
                }
            } else if (JEflag == 1)
            {
                if (getCount() < 0.5f)
                {
                    textgamer.SetActive(true);
                    textgamer.GetComponent<TextMesh>().color = new Color(1.0f, 0.9f, 0.0f, 2.0f * getCount());
                    textgamer2.SetActive(true);
                    textgamer2.GetComponent<TextMesh>().color = new Color(0.3f, 0.7f, 1.0f, 2.0f * getCount());
                    poster.SetActive(true);
                    postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, 2.0f * getCount());
                    textgamer.transform.localPosition += new Vector3(1.0f * Time.deltaTime, 1.4f * Time.deltaTime, 0.0f);
                    textgamer2.transform.localPosition += new Vector3(1.0f * Time.deltaTime, 1.4f * Time.deltaTime, 0.0f);
                    textgamer.transform.localScale = new Vector3(getCount() / 10.0f, getCount() / 10.0f, 0.06f);
                    textgamer2.transform.localScale = new Vector3(getCount() / 10.0f, getCount() / 10.0f, 0.06f);
                    placestr.SetActive(true);
                    placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 2.0f * getCount());
                }
                else if (getCount() < 1.0f)
                {
                    textcheer.SetActive(true);
                    textcheer.GetComponent<TextMesh>().color = new Color(1.0f, 0.45f, 0.4f, 2.0f * (getCount() - 0.5f));
                    textcheer.transform.localPosition += new Vector3(1.0f * Time.deltaTime, 1.4f * Time.deltaTime, 0.0f);
                    textcheer.transform.localScale = new Vector3((getCount() - 0.5f) / 10.0f, (getCount() - 0.5f) / 10.0f, 0.06f);
                }
                else if (getCount() < 1.5f)
                {
                    rogo.SetActive(true);
                    rogo.transform.localPosition += new Vector3(-2.0f * (1.25f - getCount()) * Time.deltaTime, 8.0f * (1.5f - getCount()) * Time.deltaTime, -2.0f * (1.25f - getCount()) * Time.deltaTime);
                }
                else
                {
                    webswitch.SetActive(true);
                    if (webswitchcounter <= 0.0f)
                    {
                        webswitch.transform.localScale = new Vector3(0.1f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                    }
                    else
                    {
                        webswitch.transform.localScale = new Vector3(0.02f, defaultswitchscale.y, defaultswitchscale.z);
                        webswitch.GetComponent<MeshRenderer>().material.color = new Color(0.5f, 0.5f, 0.5f);
                        webswitchcounter -= Time.deltaTime;
                    }
                    if (Input.GetMouseButtonDown(0) && webswitchcounter <= 0.0f)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit = new RaycastHit();
                        if (JEflag != -1 && Physics.Raycast(ray, out hit))
                        {
                            GameObject obj = hit.collider.gameObject;
                            if (obj == webswitch)
                            {
                                Application.OpenURL("http://www.d-vertex.com/circle/");
                                webswitchcounter = 1.0f;
                            }
                        }
                    }
                }
            }
        } 

	}

    public override void boot(int lang)
    {
        base.boot(lang);
        textgamer.transform.localPosition = gamerinitpos;
        textgamer2.transform.localPosition = gamer2initpos;
        textcheer.transform.localPosition = cheerinitpos;
        rogo.transform.localPosition = rogoinitpos;
        placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        webswitchcounter = 0.0f;
        if (lang == 0)
        {
            textgamer.GetComponent<TextMesh>().text = "ゲームを創る君も";
            textgamer2.GetComponent<TextMesh>().text = "ゲームで遊ぶ君も";
            textcheer.GetComponent<TextMesh>().text = "まとめて応援するよっ!!";
            placestr.GetComponent<TextMesh>().text = placeJ;
            webswitch.SetActive(true);
            webstr.GetComponent<TextMesh>().text = "ここをタッチ！";
            webswitch.SetActive(false);
        } else
        {
            textgamer.GetComponent<TextMesh>().text = "You who create games";
            textgamer2.GetComponent<TextMesh>().text = "and you who play games";
            textcheer.GetComponent<TextMesh>().text = "I cheer up together!!";
            placestr.GetComponent<TextMesh>().text = placeE;
            webswitch.SetActive(true);
            webstr.GetComponent<TextMesh>().text = "Touch here!";
            webswitch.SetActive(false);
        }
    }
    public override void finish()
    {
        poster.SetActive(false);
        rogo.SetActive(false);
        textgamer.SetActive(false);
        textgamer2.SetActive(false);
        textcheer.SetActive(false);
        textgamer.GetComponent<TextMesh>().color = new Color(1.0f, 0.9f, 0.0f, 0.0f);
        textgamer2.GetComponent<TextMesh>().color = new Color(0.3f, 0.7f, 1.0f, 0.0f);
        textcheer.GetComponent<TextMesh>().color = new Color(1.0f, 0.45f, 0.4f, 0.0f);
        postermesh.material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        placestr.GetComponent<TextMesh>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        placestr.SetActive(false);
        webswitch.SetActive(false);
    }
}
