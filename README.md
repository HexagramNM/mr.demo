﻿# MR Demo code
コメントアウトの部分は例。Parts which are commented out are examples.

[日本語]
ButtonE.csは英語ボタンに，ButtonJ.csは日本語ボタンにアタッチさせ，イベントハンドラの設定をすること.

"advertisementstart.cs"をAR Cameraに、"advertise1.cs"をマーカーにアタッチさせる。
("advertisementscript.cs"は"advertise1.cs"の親)

advertise1.csのみを編集

Start() 初期化(最初はアニメーションはオフ)
Update() 常に行う処理を記述(アニメーション等)。

boot()　タッチされてアニメーションを開始するときの処理
finish()　再度タッチされてアニメーションをやめる時の処理

使えるメソッド
bool getFlag()　アニメーションを動かしている状態ならtrue、そうでなければfalse。
float getCount()　アニメーションを起動してからの起動時間を返す。

[English]
Attach "advertisementstart.cs" to AR Camera, "advertise1.cs" to a marker.
("advertisementscript.cs" is the parent of "advertise1.cs".)

Edit only "advertise1.cs"

Start()  Initialization. (Animations are off.)
Update()  Process which continuously execute.

boot()　Process when you start animations with a touch. 
finish()　Process when you stop animations with a touch. 

使えるメソッド
bool getFlag()　It returns true if animations are working. Otherwise, it returns false.
float getCount()　It returns duration time of animations.