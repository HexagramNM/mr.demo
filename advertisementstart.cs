﻿using UnityEngine;
using System.Collections;

public class advertisementstart : MonoBehaviour {

    int JEflag;//0 Japanese, 1 English

	// Use this for initialization
	void Start () {
        JEflag = -1;
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (JEflag != -1 && Physics.Raycast(ray, out hit))
            {
                advertisementscript obj = hit.collider.gameObject.GetComponent<advertisementscript>();
                if (obj != null)
                {
                    obj.flagTrans();
                    if (obj.getFlag())
                    {
                        obj.boot(JEflag);
                    }
                    else
                    {
                        obj.finish();
                    }
                }
            }
        }
	}
    
    public void SetLang(char lang)
    {
        if (lang == 'J')
            JEflag = 0;
        if (lang == 'E')
            JEflag = 1;
    }
}
