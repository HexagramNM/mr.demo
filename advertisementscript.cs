﻿using UnityEngine;
using System.Collections;

public class advertisementscript : MonoBehaviour {
    private float count;
    private bool flag;
    protected int JEflag;

    // Use this for initialization
    public virtual void Start()
    {
        flag = false;
        count = 0.0f;
        JEflag = -1;
    }

    // Update is called once per frame
    public virtual void Update() {
        if (flag)
        {
            count += Time.deltaTime;
        }
    }

    public virtual void boot(int lang)
    {
        count = 0.0f;
        JEflag = lang;
    }
    public virtual void finish()
    {
    }

    public bool getFlag() { return flag; }
    public float getCount() { return count; }
    public void flagTrans()
    {
        flag = !flag;
    }
}
